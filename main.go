// Go playground
// Copyright (C) 2020  Chenxiong Qi
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/user"
	"sort"
	"strconv"
	"time"
)

// rewrite command line args using cobra

const (
	pagureIOUrl = "https://pagure.io/"
)

type PullRequestArgs struct {
	repoName *string
	onlyMy   *bool
	author   *string
	orderBy  *string
}

type IssueArgs struct {
	repoName *string
	//onlyMy bool
	author   *string
	status   *string
	assignee *string
	top      *int
}

// UserInfo representing a user info
type UserInfo struct {
	Fullname string `json:"fullname"`
	Name     string `json:"name"`
}

// Issue representing an issue info
type Issue struct {
	ID          int      `json:"id"`
	Title       string   `json:"title"`
	LastUpdated string   `json:"last_updated"`
	User        UserInfo `json:"user"`
}

// IssuesResponseBody representing the response data from remote Pagure.io
type IssuesResponseBody struct {
	TotalIssues int     `json:"total_issues"`
	Issues      []Issue `json:"issues"`
}

// PullRequest representing a pull request info
type PullRequest struct {
	ID        int      `json:"id"`
	Title     string   `json:"title"`
	UpdatedOn string   `json:"updated_on"`
	User      UserInfo `json:"user"`
}

// PullRequestsResponseBody representing the response data from remote Pagure.io
type PullRequestsResponseBody struct {
	TotalPullRequests int           `json:"total_requests"`
	PullRequests      []PullRequest `json:"requests"`
}

func httpRequest(url string, result interface{}) {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	resp.Body.Close()

	err = json.Unmarshal(content, &result)
	if err != nil {
		log.Fatalln(err)
	}
}

func convertUpdateDate(val string) time.Time {
	parsedVal, err := strconv.ParseInt(val, 10, 64)
	if err != nil {
		log.Fatalln(err)
	}
	return time.Unix(parsedVal, 0)
}

func issueHandler(args *IssueArgs) {
	if *args.repoName == "" {
		fmt.Fprintln(os.Stderr, "Missing option -repo.")
		os.Exit(1)
	}

	querystring := ""

	//if *args.onlyMy {
	//	currentUser, err := user.Current()
	//	if err != nil {
	//		log.Fatalln(err)
	//	}
	//	querystring += "author=" + currentUser.Username
	//}

	if *args.status != "" && *args.status != "open" && *args.status != "Open" {
		querystring += "status=" + *args.status
	}

	if *args.author != "" {
		querystring += "&author=" + *args.author
	}

	if *args.assignee != "" {
		querystring += "&assignee=" + *args.assignee
	}

	url := fmt.Sprintf("%sapi/0/%s/issues", pagureIOUrl, *args.repoName)
	url = fmt.Sprintf("%s?%s", url, querystring)

	var data IssuesResponseBody
	httpRequest(url, &data)

	fmt.Printf("Issues [%d]\n\n", data.TotalIssues)

	issues := data.Issues
	if *args.top > 0 {
		issues = issues[0:*args.top]
	}
	for i := range issues {
		item := issues[i]
		userInfo := item.User

		fmt.Printf("[%d] %s\n", item.ID, item.Title)
		fmt.Printf("    Created by: %s (%s) | Updated on: %s\n",
			userInfo.Fullname, userInfo.Name, convertUpdateDate(item.LastUpdated))
		fmt.Printf("    %s%s/issue/%d\n", pagureIOUrl, *args.repoName, item.ID)
		fmt.Println()
	}
}

func prHandler(args *PullRequestArgs) {
	if *args.repoName == "" {
		fmt.Fprintln(os.Stderr, "Missing option -repo.")
		os.Exit(1)
	}

	if *args.onlyMy && *args.author != "" {
		fmt.Fprintln(os.Stderr, "Both -my and -author are specified. Whose pull requests do you want?")
		os.Exit(1)
	}

	if *args.orderBy != "" {
		if *args.orderBy != "author" && *args.orderBy != "updated_on" {
			fmt.Fprintln(os.Stderr, "Only support to sort on: author, updated_on")
			os.Exit(1)
		}
	}

	url := fmt.Sprintf("%sapi/0/%s/pull-requests", pagureIOUrl, *args.repoName)

	querystring := ""

	if *args.onlyMy {
		currentUser, err := user.Current()
		if err != nil {
			log.Fatalln(err)
		}
		querystring += "author=" + currentUser.Username
	}

	if *args.author != "" {
		querystring += "author=" + *args.author
	}

	url = fmt.Sprintf("%s?%s", url, querystring)

	var data PullRequestsResponseBody
	httpRequest(url, &data)

	fmt.Printf("Pull Requests [%d]\n\n", data.TotalPullRequests)

	requests := data.PullRequests

	// Sort requests by specified field
	if *args.orderBy == "author" {
		sort.Slice(requests, func(i, j int) bool {
			return data.PullRequests[i].User.Name < data.PullRequests[j].User.Name
		})
	} else if *args.orderBy == "updated_on" {
		sort.Slice(requests, func(i, j int) bool {
			updatedOnI, err := strconv.Atoi(requests[i].UpdatedOn)
			if err != nil {
				log.Fatalln(err)
			}
			updatedOnJ, err := strconv.Atoi(requests[j].UpdatedOn)
			if err != nil {
				log.Fatalln(err)
			}
			return updatedOnI > updatedOnJ
		})
	}

	for i := range requests {
		pr := requests[i]

		fmt.Printf("[%d] %s - %s\n", pr.ID, pr.Title, pr.User.Fullname)
		fmt.Printf("    Updated on: %s\n", convertUpdateDate(pr.UpdatedOn))
		fmt.Printf("    Link: %s%s/pull-request/%d\n", pagureIOUrl, *args.repoName, pr.ID)
		fmt.Printf("    Patch: %s%s/pull-request/%d.patch\n", pagureIOUrl, *args.repoName, pr.ID)
		fmt.Println()
	}
}

func main() {
	showHelp := flag.Bool("help", false, "Show help.")

	prCmd := flag.NewFlagSet("pr", flag.ExitOnError)
	prArgs := PullRequestArgs{
		repoName: prCmd.String("repo", "", "From which repository to view pull requests."),
		onlyMy:   prCmd.Bool("my", false, "View my pull requests only."),
		author:   prCmd.String("author", "", "Show pull requests authored by a particular person."),
		orderBy:  prCmd.String("order", "", "Order pull requests by this. If not specified, the order depends on Pagure.io"),
	}

	issueCmd := flag.NewFlagSet("issue command to view issues", flag.ExitOnError)
	issueArgs := IssueArgs{
		repoName: issueCmd.String("repo", "", "From which repository to view pull requests."),
		status:   issueCmd.String("status", "", "Issue status."),
		assignee: issueCmd.String("assignee", "", "Issue assignee."),
		author:   issueCmd.String("author", "", "Show issues authored by this person."),
		top:      issueCmd.Int("top", 0, "Show top N issues."),
	}

	flag.Parse()

	if *showHelp {
		fmt.Println("program is a command line tool to interact with Pagure.io.")
		fmt.Println()
		fmt.Println("Available commands:")
		fmt.Println("    pr         Pull requests.")
		fmt.Println("    issue      Issues.")
		os.Exit(0)
	}

	if len(os.Args) < 2 {
		fmt.Fprintln(os.Stderr, "Missing subcommand. Use -help to get more help.")
		fmt.Println()
		flag.PrintDefaults()
		os.Exit(1)
	}

	switch os.Args[1] {
	case "pr":
		err := prCmd.Parse(os.Args[2:])
		if err != nil {
			log.Fatalln(err)
		}
		prHandler(&prArgs)

	case "issue":
		err := issueCmd.Parse(os.Args[2:])
		if err != nil {
			log.Fatalln(err)
		}
		issueHandler(&issueArgs)

	default:
		flag.PrintDefaults()
		os.Exit(1)
	}
}
